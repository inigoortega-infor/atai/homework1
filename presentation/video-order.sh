#!/usr/bin/env sh
player="mpv --fs --loop=inf --speed=0.5"

$player "videos/Emergent Tool Use 0b - Vision-354948732.mp4" \
    "videos/Emergent Tool Use 0c - Lidar-354948741.mp4"
$player videos/Emergent\ Tool\ Use\ 1c\ -\ Door\ Blocking-355207259.mp4
$player "videos/Emergent Tool Use 0a - Movement-354948714.mp4" \
    "videos/Emergent Tool Use 0d - Box Grab-354948752.mp4" \
    "videos/Emergent Tool Use 0e - Box Lock-354948766.mp4"
$player \
"videos/"'Emergent Tool Use 1a - Random Movement-355207231.mp4' \
"videos/"'Emergent Tool Use 1b - Chasing-355207253.mp4' \
"videos/"'Emergent Tool Use 1c - Door Blocking-355207259.mp4' \
"videos/"'Emergent Tool Use 1d - Ramp Use-355207273.mp4' \
"videos/"'Emergent Tool Use 1e - Ramp Defense-355207282.mp4' \
"videos/"'Emergent Tool Use 1f - Ramp Defense, Box Passing-355207292.mp4'
$player \
"videos/"'Emergent Tool Use 2a - Random Movement-355211280.mp4' \
"videos/"'Emergent Tool Use 2b - Chasing-355211288.mp4' \
"videos/"'Emergent Tool Use 2c - Shelter Construction-355211305.mp4' \
"videos/"'Emergent Tool Use 2d - Ramp Use-355211318.mp4' \
"videos/"'Emergent Tool Use 2e - Ramp Defense-355211334.mp4' \
"videos/"'Emergent Tool Use 2g - Surf Defense-355211351.mp4'
$player \
"videos/"'Emergent Tool Use 5a - Box Surfing-354980112.mp4' \
"videos/"'Emergent Tool Use 5b - Endless Running-354980119.mp4' \
"videos/"'Emergent Tool Use 5c - Ramp Abuse, Hiders-354980130.mp4' \
"videos/"'Emergent Tool Use 5d - Ramp Abuse, Seekers-354980134.mp4'
$player \
"videos/"'Emergent Tool Use 3a - Multiagent-355000593.mp4' \
"videos/"'Emergent Tool Use 3b - Count-Based, Partial-355000600.mp4' \
"videos/"'Emergent Tool Use 3c - Count-Based, Full-355000603.mp4'
$player \
"videos/"'Emergent Tool Use 4a - Object Counting-354992084.mp4' \
"videos/"'Emergent Tool Use 4b - Lock and Return-354992092.mp4' \
"videos/"'Emergent Tool Use 4c - Sequential Lock-354992105.mp4' \
"videos/"'Emergent Tool Use 4d - Blueprint-354992114.mp4' \
"videos/"'Emergent Tool Use 4e - Shelter-354992122.mp4'
